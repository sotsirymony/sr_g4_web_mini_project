import React, { Component } from "react";
import { Button, Dropdown, Card } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "moment/locale/km";
const moment = require("moment");
moment.locale("km");

export default class Cardinfo extends Component {
  constructor() {
    super();
    this.btnDelete = this.btnDelete.bind(this);
    this.btnUpdate = this.btnUpdate.bind(this);
    this.btnView = this.btnView.bind(this);
  }

  btnDelete = () => {
    this.props.btnDelete(this.props.personcard.id);
  };
  btnUpdate = () => {
    this.props.btnUpdate(this.props.personcard.id);
  };
  btnView = () => {
    this.props.btnView(this.props.personcard.id);
  };
  render(props) {
    const li = this.props.personcard.job.map((m) => <li>{m}</li>);

    return (
      <div className="col-2 card_element">
        <Card>
          <Dropdown style={dropColor}>
            <Dropdown.Toggle style={drop}>Action</Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item onClick={this.btnView}>View</Dropdown.Item>
              <Dropdown.Item onClick={this.btnUpdate}>Update</Dropdown.Item>
              <Dropdown.Item onClick={this.btnDelete}>Delete</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          <Card.Body>
            <Card.Title>
              <h3> {this.props.personcard.name} </h3>
            </Card.Title>
            <Card.Text>
              <h4>Job : </h4>
              {li}
            </Card.Text>
          </Card.Body>
          <Card.Footer>
            <small className="text-muted">
              {moment(this.props.personcard.update).fromNow()}
            </small>
          </Card.Footer>
        </Card>
      </div>
    );
  }
}
const drop = {
  padding: "20px",
  backgroundColor: "transparent",
  color: "blue",
  border: "0",
};

const dropColor = {
  backgroundColor: "#EDE3E9",
};
