import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Table, Form, Button, Card, Dropdown, Modal } from "react-bootstrap";
import Cardinfo from "./Cardinfo";
import List from "./List";
import Paginationn from "./Paginationn";
import Modall from "./Modall";

const myHeader = {
  padding: "10px",
  backgroundColor: "black",
  color: "white",
};
const myContainer = {
  paddingLeft: "8%",
  paddingRight: "8%",
};

const inputStyle = {
  margin: "0",
  padding: "0",
  width: "100%",
  textAlign: "left",
};
const col4_padding = {
  width: "100%",
  textAlign: "left",
  padding: "30px",
};

const labelmargin = {
  marginTop: "20px",
  marginButtom: "20px",
  fontWeight: "bold",
};
const inlineD = {
  display: "inline",
  marginLeft: "20px",
};

const btnmargin = {
  margin: "5px",
};
const labelPadding2 = { marginTop: "10px", marginBottom: "10px" };
export default class Personalinfo extends Component {
  constructor() {
    super();

    this.onSubmit = this.onSubmit.bind(this);
    this.validateAge = this.validateAge.bind(this);
    this.validateName = this.validateName.bind(this);
    this.makeid = this.makeid.bind(this);
    this.clearinput = this.clearinput.bind(this);
    this.changeCard = this.changeCard.bind(this);
    this.changeList = this.changeList.bind(this);
    this.pagination = this.pagination.bind(this);
    this.btnView = this.btnView.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.btnUpdate = this.btnUpdate.bind(this);
    this.updateFromForm = this.updateFromForm.bind(this);
    this.btnDelete = this.btnDelete.bind(this);
    this.state = {
      updateId: "",
      btnUpdating: false,
      showModal: false,
      index: 0,
      pagination: 1,
      arrpagintion: [],
      isCard: false,
      isList: true,
      job: [false, false, false],
      people: [],
      gender: "",
      modalperson: {
        id: "",
        name: "",
        age: "",
        gender: "",
        job: [],
        create: "",
        update: "",
      },
    };
  }
  changeCard = () => {
    this.setState({
      isCard: true,
      isList: false,
    });
  };
  changeList = () => {
    this.setState({
      isCard: false,
      isList: true,
    });
  };
  validateAge = (s) => {
    let patt = /[^0-9]/gi;
    if (s.match(patt) != null) {
    } else if (s === "") {
    } else {
      return false;
    }
  };
  validateName = (s) => {
    let patt = /[^a-z \s]/gi;
    if (s.match(patt) != null) {
    } else if (s === "") {
    } else {
      return false;
    }
  };
  clearinput = () => {
    this.name.value = "";
    this.age.value = "";
    document.getElementById("radio1").checked = false;
    document.getElementById("radio2").checked = false;
    document.getElementById("0").checked = false;
    document.getElementById("1").checked = false;
    document.getElementById("2").checked = false;
    this.setState({
      job: [],
      gender: "",
    });
  };

  onSubmit = (e) => {
    e.preventDefault();

    const jobList = ["student", "teacher", "developer"];
    var name = this.name.value;
    var age = this.age.value;
    let date1 = new Date();
    let ms = date1.getTime();
    let idd = this.makeid(5);

    var jobs = [];
    var i = 0;
    for (i; i < 3; i++) {
      if (this.state.job[i] === true) {
        jobs = jobs.concat(jobList[i]);
      } else {
        continue;
      }
    }

    if (
      this.validateAge(age) === false &&
      this.validateName(name) === false &&
      this.state.gender !== "" &&
      jobs.length !== 0
    ) {
      const person = {
        id: idd,
        name: name,
        age: age,
        gender: this.state.gender,
        job: jobs,
        create: ms,
        update: ms,
      };

      this.setState({
        people: this.state.people.concat(person),
      });

      let npagination = Math.ceil((this.state.people.length + 1) / 3);
      var ii = 1;
      var arrpagi = [];
      for (ii; ii <= npagination; ii++) {
        arrpagi = arrpagi.concat(ii);
      }
      this.setState({
        arrpagintion: arrpagi,
      });
    } else {
      alert("please input valid information");
    }

    this.clearinput();

    // pagintion
  };
  handleRadio = (e) => {
    var gender = e.target.value;
    this.setState((prevState) => {
      return (this.state.gender = gender);
    });
  };
  checkItem = (a) => {
    var id = a.target.id;
    var bol = this.state.job[id];
    this.setState((prevState) => {
      return (this.state.job[id] = !bol);
    });
  };
  pagination = (a) => {
    this.setState((prevState) => {
      return (this.state.pagination = a);
    });
  };

  updateFromForm = () => {
    var id = this.state.updateId;
    var name = this.name.value;
    var age = this.age.value;
    var job = [];
    var gender;

    if ((document.getElementById("radio1").checked = true)) {
      gender = "male";
    } else if ((document.getElementById("radio2").checked = true)) {
      gender = "female";
    }
    if (document.getElementById("0").checked === true) {
      job = job.concat("student");
    }
    if (document.getElementById("1").checked === true) {
      job = job.concat("teacher");
    }
    if (document.getElementById("2").checked === true) {
      job = job.concat("developer");
    }

    if (
      this.validateAge(age) === false &&
      this.validateName(name) === false &&
      gender !== "" &&
      job.length !== 0
    ) {
      var index;
      var i = 0;

      for (i; i < this.state.people.length; i++) {
        if (this.state.people[i].id === id) {
          index = i;
        }
      }

      var create = this.state.people[index].create;

      let date1 = new Date();
      let update = date1.getTime();
      const person = {
        id: id,
        name: name,
        age: age,
        gender: gender,
        job: job,
        create: create,
        update: update,
      };

      this.setState((prevState) => {
        return (
          (this.state.btnUpdating = false), (this.state.people[index] = person)
        );
      });
    } else {
      alert("please value information");
    }
    this.clearinput();
  };

  btnUpdate = (id) => {
    var index;
    var i = 0;
    for (i; i < this.state.people.length; i++) {
      if (this.state.people[i].id === id) {
        index = i;
      }
    }

    const updateperson = {
      id: this.state.people[index].id,
      name: this.state.people[index].name,
      age: this.state.people[index].age,
      gender: this.state.people[index].gender,
      job: this.state.people[index].job,
      create: this.state.people[index].create,
      update: this.state.people[index].update,
    };
    this.setState((prevState) => {
      return (
        (this.state.btnUpdating = true), (this.state.updateId = updateperson.id)
      );
    });

    this.name.value = updateperson.name;
    this.age.value = updateperson.age;
    if (updateperson.gender === "male") {
      document.getElementById("radio1").checked = true;
      document.getElementById("radio2").checked = false;
    } else if (updateperson.gender === "female") {
      document.getElementById("radio1").checked = false;
      document.getElementById("radio2").checked = true;
    }

    var ijob = 0;
    for (ijob; ijob < updateperson.job.length; ijob++) {
      if (updateperson.job[ijob] === "student") {
        document.getElementById("0").checked = true;
      } else if (updateperson.job[ijob] === "teacher") {
        document.getElementById("1").checked = true;
      } else if (updateperson.job[ijob] === "developer") {
        document.getElementById("2").checked = true;
      }
    }
  };
  btnView = (a) => {
    var index;
    var i = 0;
    for (i; i < this.state.people.length; i++) {
      if (this.state.people[i].id === a) {
        index = i;
      }
    }
    /// set modal person to show in modal
    this.setState((prevState) => {
      return (
        (this.state.index = index),
        ((this.state.showModal = true),
        (this.state.modalperson.name = this.state.people[
          this.state.index
        ].name),
        (this.state.modalperson.id = this.state.people[this.state.index].id),
        (this.state.modalperson.age = this.state.people[this.state.index].age),
        (this.state.modalperson.job = this.state.people[this.state.index].job),
        (this.state.modalperson.create = this.state.people[
          this.state.index
        ].create),
        (this.state.modalperson.update = this.state.people[
          this.state.index
        ].update),
        (this.state.modalperson.gender = this.state.people[
          this.state.index
        ].gender))
      );
    });
  };
  btnDelete = (id) => {
    // if (this.state.people.length > 1) {
    var index;
    var i = 0;
    for (i; i < this.state.people.length; i++) {
      if (this.state.people[i].id === id) {
        index = i;
      }
    }
    var pp = [...this.state.people];
    if (index !== -1) {
      pp.splice(index, 1);
      // this.setState({ people: pp });
      this.setState((prevState) => {
        return (this.state.people = pp);
      });
    }

    let n = this.state.people.length - 1;
    let npagination = Math.ceil(n / 3);
    var ii = 1;
    var arrpagi = [];

    for (ii; ii <= npagination; ii++) {
      arrpagi = arrpagi.concat(ii);
    }

    if (this.state.pagination > npagination) {
      var changepagination = this.state.pagination - 1;
      this.setState({ pagination: changepagination });
    }
    this.setState({
      arrpagintion: arrpagi,
    });
  };

  closeModal = () => {
    this.setState((prevState) => {
      return (this.state.showModal = false);
    });
  };

  makeid = (length) => {
    var i;
    var count = 0;
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;

    for (i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    for (i = 0; i < this.state.people.length; i++) {
      if (result === this.state.people[i].id) {
        count++;
      }
    }
    if (count !== 0) {
      this.makeid(5);
    }

    return result;
  };
  // -NAVUTH
  render() {
    ///////////////////////////HEAD
    const person = this.state.people.map((p, index) =>
      index + 1 >= this.state.pagination * 3 - 2 &&
      index < this.state.pagination * 3 ? (
        <List
          person={p}
          key={p.id}
          btnView={this.btnView}
          btnUpdate={this.btnUpdate}
          btnDelete={this.btnDelete}
        />
      ) : null
    );
    const personcard = this.state.people.map((p) => (
      <Cardinfo
        btnDelete={this.btnDelete}
        btnView={this.btnView}
        btnUpdate={this.btnUpdate}
        personcard={p}
        key={p.id}
      />
    ));
    const list = (
      <div className="col-12">
        <Table responsive>
          <thead>
            <tr>
              <th>id</th>
              <th>Name</th>
              <th>Age</th>
              <th>Gender</th>
              <th>Jobs</th>
              <th>Created At</th>
              <th>Updated At</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{person}</tbody>
        </Table>
      </div>
    );

    const card = (
      <div className="container-fluid" style={myContainer}>
        <div className="row">{personcard}</div>
      </div>
    );

    const m = (
      <Modall
        person={this.state.modalperson}
        isModal={this.state.showModal}
        closeModal={this.closeModal}
      />
    );
    return (
      <div>
        {m}
        <div className="container-fluid" style={myContainer}>
          <div className="row">
            <div className="col-12" style={myHeader}>
              <h1>Personal Information</h1>
            </div>
            <div className="col-8" style={inputStyle}>
              <Form.Group>
                <Form.Label style={labelmargin}>Name :</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Input Name"
                  ref={(name) => (this.name = name)}
                />
                <Form.Label style={labelmargin}>Age :</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Age"
                  ref={(age) => (this.age = age)}
                />
              </Form.Group>
              {this.state.btnUpdating === false ? (
                <Button
                  type="submit"
                  className="btn btn-dark"
                  onClick={this.onSubmit}
                >
                  Submit
                </Button>
              ) : (
                <Button
                  className="btn btn-warning"
                  onClick={this.updateFromForm}
                >
                  Update
                </Button>
              )}
            </div>
            <div className="col-4" style={col4_padding}>
              <h3 style={labelPadding2}>Gender:</h3>
              <Form.Check
                style={inlineD}
                type="radio"
                label="Male"
                name="gender"
                id="radio1"
                value="male"
                onChange={this.handleRadio}
              />
              <Form.Check
                style={inlineD}
                type="radio"
                label="Female"
                name="gender"
                id="radio2"
                value="female"
                onChange={this.handleRadio}
              />
              <h3 style={labelPadding2}>Job:</h3>
              {["checkbox"].map((type) => (
                <div key={`inline-${type}`} className="mb-3">
                  <Form.Check
                    inline
                    label="Student"
                    type={type}
                    id="0"
                    onChange={(e) => this.checkItem(e)}
                  />
                  <Form.Check
                    inline
                    label="Teacher"
                    type={type}
                    id="1"
                    onChange={(e) => this.checkItem(e)}
                  />
                  <Form.Check
                    inline
                    label="Developer"
                    type={type}
                    id="2"
                    onChange={(e) => this.checkItem(e)}
                  />
                </div>
              ))}
            </div>

            <div className="col-12">
              <h5 style={inlineD}>display data : </h5>
              <Button
                variant="outline-dark"
                style={btnmargin}
                onClick={this.changeList}
              >
                List
              </Button>
              <Button variant="outline-dark" onClick={this.changeCard}>
                Card
              </Button>
            </div>
            {this.state.isList === true ? list : null}
            {this.state.isCard === true ? card : null}
            {this.state.isList === true ? (
              <div className="pagination mx-auto">
                <Paginationn
                  alldata={this.state.arrpagintion}
                  pagi={this.pagination}
                  forActive={this.state.pagination}
                />
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}
